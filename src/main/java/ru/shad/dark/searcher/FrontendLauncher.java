package ru.shad.dark.searcher;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.shad.dark.api.services.searcher.SearchResults;
import ru.shad.dark.api.services.searcher.searcherServiceFrontend;
import ru.shad.dark.searcher.service.LocalRepository;
import ru.shad.dark.searcher.service.SearcherFrontend;

/**
 * User: dark
 * Date: 10.05.12
 * Time: 20:14
 */
public class FrontendLauncher {
    private static Logger logger = LoggerFactory.getLogger(FrontendLauncher.class);

    public static void main(String[] args) throws TException {
        VersionInfo.GitRepoState gitRepoState=VersionInfo.gitRepositoryState;
        if (args.length != 1) {
            logger.error("args should be <frontendPort>");
            System.exit(1);
        }

        TNonblockingServerSocket transport = new TNonblockingServerSocket(Integer.parseInt(args[0]));
        searcherServiceFrontend.Processor processor = new searcherServiceFrontend.Processor(new SearcherFrontend());
        TServer server = new THsHaServer(new THsHaServer.Args(transport).processor(processor));
        transport.listen();
        logger.info("Starting server at port " + args[0]);

        try {
            server.serve();
        } catch (Exception e) {
            server.stop();
            logger.info("Stopped server", e);
        }
        transport.close();
    }
}
