package ru.shad.dark.searcher.service;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.shad.dark.api.services.searcher.SearchResults;
import ru.shad.dark.api.services.searcher.SearcherService;
import ru.shad.dark.api.services.searcher.searcherServiceFrontend;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * User: dark
 * Date: 11.05.12
 * Time: 0:23
 */
public class SearcherBackend implements SearcherService.Iface {
    private static final Logger logger = LoggerFactory.getLogger(SearcherBackend.class);
    private final LocalRepository repository;

    public SearcherBackend(LocalRepository repository, String registerHost, int registerPort, String ownHost, int ownPort) {
        this.repository = repository;
        boolean registered = false;
        TSocket socket= new TSocket(registerHost, registerPort);
        TFramedTransport transport=new TFramedTransport(socket);
        TBinaryProtocol protocol = new TBinaryProtocol(transport);

        searcherServiceFrontend.Client client = new searcherServiceFrontend.Client(protocol);
        try {
            socket.open();
            registered = client.addBackend(ownHost,ownPort);
            if(!registered) throw new RuntimeException("Registration denied");
        } catch (TException e) {
            throw new RuntimeException("Failed to register", e);
        }
    }

    @Override
    public SearchResults search(String word) throws TException {
        SearchResults results = new SearchResults(new ArrayList<String>());
        Collection<String> result = repository.get(word);
        for (String s : result) {
            results.getResults().add(s);
        }
        return results;
    }

    @Override
    public boolean ping() throws TException {
        logger.info("Ping request");
        return true;
    }
}
