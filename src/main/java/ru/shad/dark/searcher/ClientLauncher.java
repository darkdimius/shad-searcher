package ru.shad.dark.searcher;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.shad.dark.api.services.searcher.SearchResults;
import ru.shad.dark.api.services.searcher.searcherServiceFrontend;

import javax.naming.directory.SearchResult;

/**
 * User: dark
 * Date: 10.05.12
 * Time: 20:14
 */
public class ClientLauncher {
    private static Logger logger = LoggerFactory.getLogger(ClientLauncher.class);

    public static void main(String[] args) throws TException {
        VersionInfo.GitRepoState gitRepoState=VersionInfo.gitRepositoryState;
        if (args.length != 2) {
            logger.error("args should be <frontendHost> <frontendPort>");
            System.exit(1);
        }
        TSocket socket = new TSocket(args[0], Integer.parseInt(args[1]));
        TFramedTransport transport=new TFramedTransport(socket);
        try {
            TProtocol protocol = new TBinaryProtocol(transport);

            transport.open();
            searcherServiceFrontend.Client client = new searcherServiceFrontend.Client(protocol);
            logger.info("running query");
            SearchResults result = client.search("test_query");
            for (String s : result.getResults()) {
                logger.info("result retrieved {}", s);
            }
        } finally {
            transport.close();
        }
    }
}
