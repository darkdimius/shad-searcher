namespace java ru.shad.dark.api.services.searcher

struct SearchResults {
    1: required list<string> results;

}

service searcherServiceFrontend {
    SearchResults search (
        1: required string query;
    ),
    bool addBackend (
        1: string address;
        2: i32 port;
    );
}
service SearcherService {
    SearchResults search (
        1: required string word;
    ),
    bool ping();
}