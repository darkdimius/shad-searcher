package ru.shad.dark.searcher.service;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.shad.dark.api.services.searcher.SearchResults;
import ru.shad.dark.api.services.searcher.SearcherService;
import ru.shad.dark.api.services.searcher.searcherServiceFrontend;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * User: dark
 * Date: 11.05.12
 * Time: 0:05
 */
public class SearcherFrontend implements searcherServiceFrontend.Iface {
    private static final Logger logger = LoggerFactory.getLogger(SearcherFrontend.class);

    private final CopyOnWriteArraySet<BackendDescription> backends;

    private final Pinger pingingThread;

    public SearcherFrontend() {
        backends = new CopyOnWriteArraySet<BackendDescription>();
        pingingThread = new Pinger();
        pingingThread.setName("Pinging thread for SearcherService#" + this.hashCode());
        pingingThread.start();
    }

    public void stop() throws InterruptedException {
        pingingThread.stop=true;
        pingingThread.join();
    }

    @Override
    public SearchResults search(String query) throws TException {
        SearchResults results = new SearchResults(new ArrayList<String>());
        for (Iterator<BackendDescription> iterator = backends.iterator(); iterator.hasNext(); ) {
            BackendDescription backend = iterator.next();
            TSocket socket = new TSocket(backend.host, backend.port);
            TFramedTransport transport=new TFramedTransport(socket);
            TProtocol protocol = new TBinaryProtocol(transport);
            SearcherService.Client client = new SearcherService.Client(protocol);
            SearchResults backendResult;
            try {
                transport.open();
                backendResult = client.search(query);
            } catch (TException e) {
                logger.info("Removing " + backend, e);
                backends.remove(backend);
                continue;
            }
            transport.close();
            results.getResults().addAll(backendResult.getResults());
        }
        return results;
    }

    @Override
    public boolean addBackend(String address, int port) throws TException {
        backends.add(new BackendDescription(port,address));
        return true;
    }


    private final class BackendDescription {
        public final int port;
        public final String host;

        private BackendDescription(int port, String host) {
            this.port = port;
            this.host = host;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }

            BackendDescription that = (BackendDescription) o;

            if (port != that.port) {
                return false;
            }
            if (host != null ? !host.equals(that.host) : that.host != null) {
                return false;
            }

            return true;
        }

        @Override
        public int hashCode() {
            int result = port;
            result = 31 * result + (host != null ? host.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "BackendDescription{" +
                   "port=" + port +
                   ", host='" + host + '\'' +
                   '}';
        }
    }

    private final class Pinger extends Thread {
        public volatile boolean stop;

        @Override
        public void run() {
            while (!stop) {
                for (Iterator<BackendDescription> iterator = backends.iterator(); iterator.hasNext(); ) {
                    BackendDescription backend = iterator.next();
                    TSocket socket = new TSocket(backend.host, backend.port);
                    TFramedTransport transport=new TFramedTransport(socket);
                    TBinaryProtocol protocol = new TBinaryProtocol(transport);
                    SearcherService.Client client = new SearcherService.Client(protocol);
                    try {
                        transport.open();
                        client.ping();
                    } catch (TException e) {
                        logger.info("Removing " + backend, e);
                        backends.remove(backend);
                    }
                    transport.close();
                }
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }
}
