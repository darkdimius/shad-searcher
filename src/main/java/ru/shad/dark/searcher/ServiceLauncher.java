package ru.shad.dark.searcher;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.shad.dark.api.services.searcher.SearcherService;
import ru.shad.dark.api.services.searcher.searcherServiceFrontend;
import ru.shad.dark.searcher.service.LocalRepository;
import ru.shad.dark.searcher.service.SearcherBackend;
import ru.shad.dark.searcher.service.SearcherFrontend;

import java.net.UnknownHostException;

/**
 * User: dark
 * Date: 10.05.12
 * Time: 20:14
 */
public class ServiceLauncher {
    private static Logger logger = LoggerFactory.getLogger(ServiceLauncher.class);

    public static void main(String[] args) throws TTransportException, UnknownHostException {
        VersionInfo.GitRepoState gitRepoState=VersionInfo.gitRepositoryState;
        if (args.length != 4) {
            logger.error("args should be <frontendHost> <frontendPort> <localInterfaceIp> <localPort>");
            System.exit(1);
        }
        LocalRepository stub = new LocalRepository();
        stub.add("test_query", java.net.InetAddress.getLocalHost()+args[3]);
        TNonblockingServerSocket transport = new TNonblockingServerSocket(Integer.parseInt(args[3]));
        logger.info("Registering at " +args[0]+":"+Integer.parseInt(args[1]));
        SearcherService.Processor processor = new SearcherService.Processor(
                new SearcherBackend(stub, args[0], Integer.parseInt(args[1]), args[2], Integer.parseInt(args[3]))
        );
        TServer server = new THsHaServer(new THsHaServer.Args(transport).processor(processor));

        logger.info("Starting server at port " + args[0]);

        try {
            server.serve();
        } catch (Exception e) {
            server.stop();
            logger.info("Stopped server", e);
        }
        transport.close();
    }
}
