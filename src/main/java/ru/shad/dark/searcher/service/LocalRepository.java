package ru.shad.dark.searcher.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

/**
 * User: dark
 * Date: 11.05.12
 * Time: 0:10
 */

/**
 * an efficient concurrent multimap
 */
public class LocalRepository {
    private static final Logger logger = LoggerFactory.getLogger(LocalRepository.class);

    private ConcurrentMap data = new ConcurrentHashMap<String, ConcurrentLinkedQueue<String>>();

    public void add(String key, String value) {
        ConcurrentLinkedQueue<String> queue = (ConcurrentLinkedQueue<String>) data.get(key);
        if (queue == null) {
            queue = new ConcurrentLinkedQueue<String>();
            ConcurrentLinkedQueue<String> oldQueue = (ConcurrentLinkedQueue<String>) data.putIfAbsent(key, queue);
            if (oldQueue != null) {
                queue = oldQueue;
            }

        }
        if (!queue.contains(value)) {
            queue.add(value);
        }
    }

    public Collection<String> get(String key) {
        Queue<String> queue = (ConcurrentLinkedQueue<String>) data.get(key);
        if (queue == null) {
            queue = new LinkedList<String>();
        }
        return Collections.unmodifiableCollection(queue);
    }

    public Set<String> getKeyPrefixes() {
        Set<String> result=new HashSet<String>();
        for (Object o : data.keySet()) {
            String s = (String) o;
            result.add(Character.toString(s.charAt(0)));
        }
        return result;
    }

}
