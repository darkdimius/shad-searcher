package ru.shad.dark.searcher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * User: dark
 * Date: 10.05.12
 * Time: 20:54
 */
public final class VersionInfo {
    private static final Logger logger = LoggerFactory.getLogger(VersionInfo.class);
    public static final GitRepoState gitRepositoryState;

    static {
        {
            Properties properties = new Properties();
            try {
                properties.load(VersionInfo.class.getClassLoader().getResourceAsStream("git.properties"));
            } catch (IOException e) {
                logger.error("Failed to read version info", e);
                throw new Error("Failed to read version info", e);
            }

            gitRepositoryState = new GitRepoState(properties);
            logger.info("revision " + VersionInfo.gitRepositoryState.commitId);
            logger.info("buildTime " + VersionInfo.gitRepositoryState.buildTime);
        }
    }


    /**
     * User: dark
     * Date: 10.05.12
     * Time: 20:56
     */
    public static final class GitRepoState {
        private static Logger logger = LoggerFactory.getLogger(GitRepoState.class);
        public final String branch;                  // =${git.branch}
        public final String commitId;                // =${git.commit.id}
        public final String commitIdAbbrev;          // =${git.commit.id.abbrev}
        public final String buildUserName;           // =${git.build.user.name}
        public final String buildUserEmail;          // =${git.build.user.email}
        public final String buildTime;               // =${git.build.time}
        public final String commitUserName;          // =${git.commit.user.name}
        public final String commitUserEmail;         // =${git.commit.user.email}
        public final String commitMessageFull;       // =${git.commit.message.full}
        public final String commitMessageShort;      // =${git.commit.message.short}
        public final String commitTime;              // =${git.commit.time}


        public GitRepoState(Properties properties)
        {
            this.branch = properties.get("git.branch").toString();
            this.commitId = properties.get("git.commit.id").toString();
            this.buildUserName = properties.get("git.build.user.name").toString();
            this.buildUserEmail = properties.get("git.build.user.email").toString();
            this.buildTime = properties.get("git.build.time").toString();
            this.commitUserName = properties.get("git.commit.user.name").toString();
            this.commitUserEmail = properties.get("git.commit.user.email").toString();
            this.commitMessageShort = properties.get("git.commit.message.short").toString();
            this.commitMessageFull = properties.get("git.commit.message.full").toString();
            this.commitTime = properties.get("git.commit.time").toString();
            this.commitIdAbbrev=properties.get("git.commit.id.abbrev").toString();
        }
    }
}

